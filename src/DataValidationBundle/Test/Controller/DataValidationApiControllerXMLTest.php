<?php


namespace DataValidationBundle\Test\Controller;

use Core\Api\DefaultKeyRepository;
use Core\ContactData\ContactData;
use Core\CreditCard\CreditCard;
use Core\Validation\Validators\HashKeyValidation;
use DataValidationBundle\Model\TransactionData;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class DataValidationApiControllerXMLTest extends WebTestCase {


    private $haskKey;
    private $client;
    public function setUp()
    {
        $this->client = static::createClient(array());
        $this->haskKey = new HashKeyValidation(new DefaultKeyRepository());
    }



    public function testXMLValidData(){
        $xmlArray = json_decode(json_encode((array)simplexml_load_string($this->xml())),1);;

        $hash = $this->haskKey->generateHash($xmlArray);

        $this->client->request(
            'POST',
            '/api/v1/data/transactiondatas.json',
            array(),
            array(),
            array('CONTENT_TYPE' => 'text/xml'),
            sprintf($this->xml(),$hash)
        );


        $this->assertXMLResponse($this->client->getResponse(), 200, false);
        $this->assertContains('true',$this->client->getResponse()->getContent());
    }

    public function testXMLInValidHashKeyData(){
        $this->client->request(
            'POST',
            '/api/v1/data/transactiondatas.json',
            array(),
            array(),
            array('CONTENT_TYPE' => 'text/xml'),
            $this->xml()
        );
        $this->assertXMLResponse($this->client->getResponse(), 403, false);
        $this->assertContains('Invalid hash key',$this->client->getResponse()->getContent());
    }
    protected function assertXMLResponse($response, $statusCode = 200, $checkValidJson =  true, $contentType = 'application/json')
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(),
            $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', $contentType),
            $response->headers
        );
        if ($checkValidJson) {
            $decode = json_decode($response->getContent());
            $this->assertTrue(($decode != null && $decode != false),
                'is response valid xml: [' . $response->getContent() . ']'
            );
        }
    }

    protected function xml(){
        return file_get_contents(__DIR__."/test.xml");
    }


}