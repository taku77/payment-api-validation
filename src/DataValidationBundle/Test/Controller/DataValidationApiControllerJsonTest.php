<?php


namespace DataValidationBundle\Test\Controller;

use Core\Api\DefaultKeyRepository;
use Core\ContactData\ContactData;
use Core\CreditCard\CreditCard;
use Core\Validation\Validators\HashKeyValidation;
use DataValidationBundle\Model\TransactionData;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class DataValidationApiControllerJsonTest extends WebTestCase {


    private $haskKey;
    private $client;
    public function setUp()
    {
        $this->client = static::createClient(array());
        $this->haskKey = new HashKeyValidation(new DefaultKeyRepository());
    }



    public function testJsonValidData(){
        $hash = $this->haskKey->generateHash(json_decode($this->json(), true));

        $this->client->request(
            'POST',
            '/api/v1/data/transactiondatas.json',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            sprintf($this->json(),$hash)
        );

        $this->assertJsonResponse($this->client->getResponse(), 200, false);
        $this->assertContains('true',$this->client->getResponse()->getContent());
    }

    public function testJsonInValidHashKeyData(){
        $this->client->request(
            'POST',
            '/api/v1/data/transactiondatas.json',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $this->json()
        );
        $this->assertJsonResponse($this->client->getResponse(), 403, false);
        $this->assertContains('Invalid hash key',$this->client->getResponse()->getContent());
    }
    protected function assertJsonResponse($response, $statusCode = 200, $checkValidJson =  true, $contentType = 'application/json')
    {
        $this->assertEquals(
            $statusCode, $response->getStatusCode(),
            $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', $contentType),
            $response->headers
        );
        if ($checkValidJson) {
            $decode = json_decode($response->getContent());
            $this->assertTrue(($decode != null && $decode != false),
                'is response valid json: [' . $response->getContent() . ']'
            );
        }
    }

    protected function json(){
        return '{
                  "transactiondata": {
                    "credit_card": {
                      "card_number": "4485297189383563",
                      "ccv2" : 123,
                      "expiration_date" : "09/19"
                    },
                    "contact_data":{
                      "phone_number": "555555555",
                      "mobile" : "234233234",
                      "email" : "example@example.com"
                    }
                  },
                  "hash": "%s",
                  "timestamp" : "2017-02-01T18:25:43.511Z"
        }';
    }
}