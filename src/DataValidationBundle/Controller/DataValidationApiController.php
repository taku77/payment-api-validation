<?php
namespace DataValidationBundle\Controller;
use Core\CreditCard\CreditCard;
use DataValidationBundle\Form\TransactionDataType;
use DataValidationBundle\Model\TransactionData;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DataValidationApiController
 * @package DataValidationBundle\Controller
 */
class DataValidationApiController extends FOSRestController  {

    /**
     * @param Request $request
     * @return mixed
     * @ApiDoc(
     *      description="This method validate transaction data",
     *      input="DataValidationBundle\Form\TransactionDataType"
     *
     * )
     */
    public function postTransactiondataAction(Request $request){
        return $this->get('validation.transactiondata.handler')->post($request);
    }
}