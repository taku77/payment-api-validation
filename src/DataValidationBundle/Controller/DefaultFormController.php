<?php
namespace DataValidationBundle\Controller;
use DataValidationBundle\Form\TransactionDataType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: tomek
 * Date: 26.01.17
 * Time: 22:53
 */

class DefaultFormController extends Controller{
    public function indexAction(Request $request){
        $form = $this->createForm(new TransactionDataType());

        $this->get('validation.transactiondata.handler')->post($request);
        return $this->render('default/index.html.twig',
            array('form' => $form->createView())
        );
    }
}