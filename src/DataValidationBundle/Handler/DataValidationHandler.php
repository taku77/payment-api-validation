<?php
namespace DataValidationBundle\Handler;
use Core\Validation\Validatable;
use Core\Validation\Validator;
use Core\Validation\Validators\HashKeyValidation;
use DataValidationBundle\Form\TransactionDataType;
use DataValidationBundle\Model\TransactionData;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;


class DataValidationHandler{
    /**
     * @var Validatable
     */
    private $validatable;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var AbstractType
     */
    private $formType;

    /**
     * @var HashKeyValidation
     */
    private $hashKeyValidation;


    /**
     * DataValidationHandler constructor.
     * @param Validatable $validatable
     * @param AbstractType $formType
     * @param FormFactoryInterface $formFactory
     * @param HashKeyValidation $hashKeyValidation
     */
    public function __construct(Validatable $validatable,AbstractType $formType,FormFactoryInterface $formFactory, HashKeyValidation $hashKeyValidation)
    {
        $this->formType = $formType;
        $this->formFactory = $formFactory;
        $this->validatable = $validatable;
        $this->hashKeyValidation = $hashKeyValidation;
    }

    /**
     * @param Request $params
     * @return mixed
     */
    public function post(Request $params){

        return $this->validateData($params, "POST");
    }
    /**
     * @param Request $params
     * @return mixed
     */
    public function get(Request $params){
        return $this->validateData($params);
    }
    /**
     * @param Request $params
     * @param string $method
     * @return mixed
     */
    private function validateData(Request $params, $method = "GET")
    {
        $hashKeyCheck = $this->hashKeyValidation->validateData($params->request->all());
        if($hashKeyCheck === true){
            $form = $this->formFactory->create( $this->formType, $this->validatable, array('method' => $method));
            $form->handleRequest($params);
            $data = $form->getData();
            if($data instanceof Validatable)
                return View::create($data->validate(),200);

        }
        return View::create(array('errors' => $hashKeyCheck),403);
    }
}