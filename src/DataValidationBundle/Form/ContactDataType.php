<?php
namespace DataValidationBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ContactDataType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',TextType::class,array('required' => true))
            ->add('mobile',TextType::class,array('required' => true))
            ->add('phoneNumber',TextType::class,array('required' => true));

    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Core\ContactData\ContactData'
        ));
    }
    public function getBlockPrefix()
    {
        return 'contactdata';
    }
}