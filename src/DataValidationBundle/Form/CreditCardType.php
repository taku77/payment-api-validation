<?php
namespace DataValidationBundle\Form;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CreditCardType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cardNumber',TextType::class,array('required' => true))
            ->add('ccv2',IntegerType::class,array('required' => true, 'description' => '\d+'))
            ->add('expirationDate',TextType::class,array('required' => true));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Core\CreditCard\CreditCard'
        ));
    }

    public function getBlockPrefix()
    {
        return 'creditcard';
    }
}