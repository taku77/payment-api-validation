<?php
namespace DataValidationBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class TransactionDataType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contactData',ContactDataType::class)
            ->add('creditCard',CreditCardType::class)
            ->add('timestamp',TextType::class)
            ->add('hash',TextType::class);


    }
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DataValidationBundle\Model\TransactionData',
            'csrf_protection' => false
        ));
    }
    public function getBlockPrefix()
    {
        return 'transactiondata';
    }
}