<?php
namespace DataValidationBundle\Model;
use Core\Api\ApiObject;
use Core\ContactData\ContactData;
use Core\CreditCard\CreditCard;
use Core\Validation\Validatable;

/**
 * Class TransactionData
 * @package DataValidationBundle\Model
 */
class TransactionData extends ApiObject  implements Validatable {
    /**
     * @var ContactData
     */
    private $contactData;
    /**
     * @var CreditCard
     */
    private $creditCard;

    public function __construct()
    {
        $this->contactData = new ContactData();
        $this->creditCard = new CreditCard();
    }

    /**
     * @return ContactData
     */
    public function getContactData(){
        return $this->contactData;
    }

    /**
     * @return CreditCard
     */
    public function getCreditCard(){
        return $this->creditCard;
    }

    /**
     * @param ContactData $contactData
     */
    public function setContactData(ContactData $contactData)
    {
        $this->contactData = $contactData;
    }

    /**
     * @param CreditCard $creditCard
     */
    public function setCreditCard(CreditCard $creditCard)
    {
        $this->creditCard = $creditCard;
    }

    /**
     * @return array|boolean
     */
    public function validate()
    {
        $errors = array();
        $creditCardValidation = $this->creditCard->validate();
        $contactData = $this->contactData->validate() ;
        $creditCardValidation ? array_push($errors, $creditCardValidation) : null;
        $contactData ? array_push($errors, $contactData) : null;
        return sizeof($errors) ? array('errors' => $errors ): true;

    }
}