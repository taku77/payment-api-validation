<?php
namespace Core\ContactData;


use Core\Validation\Validatable;
use Core\Validation\Validator;
use Core\Validation\Validators\EmailValidation;
use Core\Validation\Validators\PhoneNumberValidation;
use Core\Validation\Validators\RequiredValidation;

/**
 * Class ContactData
 * @package Core\ContactData
 */
class ContactData implements Validatable {
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $mobile;
    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }


    /**
     * @return array
     */
    public function validate()
    {
        $validationResult = array();
        $this->phoneNumberValidator($validationResult);
        $this->emailValidator($validationResult);
        $this->mobileValidator($validationResult);
        return sizeof($validationResult) ?  $validationResult : null;
    }

    /**
     * @param array $validationResult
     */
    public function phoneNumberValidator(array &$validationResult){
        $phoneNumberValidator = new Validator($this->getPhoneNumber());
        $phoneNumberValidator->addValidation(new PhoneNumberValidation());
        $phoneNumberValidator->addValidation(new RequiredValidation());
        $phoneNumberErrors = $phoneNumberValidator->validate();
        if($phoneNumberValidator->getErrorLevel())
            $validationResult[$this->getClassName()][0]['phonenumber'] = $phoneNumberErrors;
    }

    /**
     * @param array $validationResult
     */
    public function emailValidator(array &$validationResult){
        $emailValidator = new Validator($this->getEmail());
        $emailValidator->addValidation(new EmailValidation());
        $emailValidator->addValidation(new RequiredValidation());
        $emailErrors = $emailValidator->validate();
        if($emailValidator->getErrorLevel())
            $validationResult[$this->getClassName()][0]['email'] = $emailErrors;
    }

    /**
     * @param array $validationResult
     */
    public function mobileValidator(array &$validationResult){
    $mobileValidator = new Validator($this->getMobile());
    $mobileValidator->addValidation(new PhoneNumberValidation());
    $mobileValidator->addValidation(new RequiredValidation());
    $mobileErrors = $mobileValidator->validate();
    if($mobileValidator->getErrorLevel())
        $validationResult[$this->getClassName()][0]['mobile'] = $mobileErrors;
    }
    /**
     * @return string
     */
    private function getClassName(){
        return 'contactdata';
    }
}