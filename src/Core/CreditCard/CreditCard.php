<?php
namespace Core\CreditCard;
use Core\Validation\Validator;
use Core\Validation\Validators\CardExpirationDateValidation;
use Core\Validation\Validators\CardNumberLenValidation;
use Core\Validation\Validators\CCV2Validation;
use Core\Validation\Validatable;
use Core\Validation\Validators\LuhnAlgorithmValidation;
use Core\Validation\Validators\RequiredValidation;


/**
 * Class CreditCard
 * @package Core\CreditCard
 */
class CreditCard implements Validatable{
    /**
     * @var string
     */
    private $cardNumber;
    /**
     * @var integer
     */
    private $ccv2;
    /**
     * @var string
     */
    private $expirationDate;

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return int
     */
    public function getCcv2()
    {
        return $this->ccv2;
    }

    /**
     * @param int $ccv2
     */
    public function setCcv2($ccv2)
    {
        $this->ccv2 = $ccv2;
    }

    /**
     * @return string
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param string $expirationDate
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    /**
     * @return null|array
     */
    public function validate()
    {

        $validationResult = array();
        $this->ccv2Validator($validationResult);
        $this->expirationDateValidator($validationResult);
        $this->cardNumberValidator($validationResult);

        return sizeof($validationResult) ?  $validationResult : null;

    }

    /**
     * @param array $validationResult
     */
    public function ccv2Validator(array &$validationResult){
        $ccv2Validation = new Validator($this->getCcv2());
        $ccv2Validation->addValidation(new RequiredValidation());
        $ccv2Validation->addValidation(new CCV2Validation());
        $ccv2Errors = $ccv2Validation->validate();
        if($ccv2Validation->getErrorLevel())
            $validationResult[$this->getClassName()][0]['ccv2'] = $ccv2Errors;
    }
    /**
     * @param array $validationResult
     */
    public function cardNumberValidator(array &$validationResult){
        $cardNumberValidation = new Validator($this->getCardNumber());
        $cardNumberValidation->addValidation(new RequiredValidation());
        $cardNumberValidation->addValidation(new CardNumberLenValidation());
        $cardNumberValidation->addValidation(new LuhnAlgorithmValidation());

        $cardNumberErrors = $cardNumberValidation->validate();
        if($cardNumberValidation->getErrorLevel())
            $validationResult[$this->getClassName()][0]['cardnumber'] = $cardNumberErrors;
    }

    /**
     * @param array $validationResult
     */
    public function expirationDateValidator(array &$validationResult){
        $expirationDateValidation = new Validator($this->getExpirationDate());
        $expirationDateValidation->addValidation(new CardExpirationDateValidation());
        $expirationDateValidation->addValidation(new RequiredValidation());
        $expirationDateErrors= $expirationDateValidation->validate();
        if($expirationDateValidation->getErrorLevel())
            $validationResult[$this->getClassName()][0]['expirationdate'] = $expirationDateErrors;
    }

    /**
     * @return string
     */
    private function getClassName(){
        return 'creditcard';
    }
}