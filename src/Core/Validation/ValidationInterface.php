<?php
namespace Core\Validation;



interface ValidationInterface{
    /**
     * @param mixed $data
     */
    public function validateData($data);
}