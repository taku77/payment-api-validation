<?php
namespace Core\Validation;


class Validator{
    /**
     * @var ValidationInterface[]
     */
    private $validators;
    /**
     * @var bool
     */
    private $errorLevel = false;

    /**
     * @var mixed
     */
    private $validationData;
    /**
     * Validator constructor.
     * @param $validationData
     */
    public function __construct($validationData)
    {
        $this->validationData = $validationData;
    }

    /**
     * @param ValidationInterface $validationInterface
     */
    public function addValidation(ValidationInterface $validationInterface){
        $this->validators[] = $validationInterface;
    }

    /**
     * @return array
     * @return void
     */
    public function validate(){

        $validation = array();
        /** @var ValidationInterface $validator */
        foreach ($this->validators as $validator){
            $validate = $validator->validateData($this->validationData);
            if($validate)
            {
                $validation[] = $validate;
                $this->errorLevel = true;
            }

        }

        if($this->getErrorLevel())
            return $validation;
    }

    /**
     * @return bool
     */
    public function getErrorLevel(){
        return $this->errorLevel;
    }
}