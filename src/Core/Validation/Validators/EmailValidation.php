<?php
namespace Core\Validation\Validators;
use Core\Validation\ValidationInterface;

/**
 * Class EmailValidation
 * @package Core\Validation
 */
final class EmailValidation implements ValidationInterface {
    /**
     * @param mixed $data
     * @return bool
     */
    public function validateData($data)
    {
        if(!filter_var($data,FILTER_VALIDATE_EMAIL) )
            return 'Invalid email format';

    }
}