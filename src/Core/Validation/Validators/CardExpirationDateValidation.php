<?php
namespace Core\Validation\Validators;

use Core\Validation\ValidationInterface;

/**
 * Class CardExpirationDateValidation
 * @package Core\Validation\Validators
 */
final class CardExpirationDateValidation implements ValidationInterface {

    /**
     * {@inheritdoc}
     */
    public function validateData($data)
    {
        if(!preg_match('/^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/',$data))
                return 'Invalid expiration date format';
    }
}