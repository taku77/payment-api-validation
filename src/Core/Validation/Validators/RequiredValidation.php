<?php
namespace Core\Validation\Validators;
use Core\Validation\ValidationInterface;


final class RequiredValidation implements ValidationInterface  {

    /**
     * {@inheritdoc}
     */
    public function validateData($data)
    {
        if(!$data)
            return 'This field is required';
    }
}