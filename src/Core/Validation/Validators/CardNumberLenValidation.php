<?php
namespace Core\Validation\Validators;
use Core\Validation\ValidationInterface;

/**
 * Class CardNumberLenValidation
 * @package Core\Validation
 */
final class CardNumberLenValidation implements ValidationInterface {

    /**
     * {@inheritdoc}
     */
    public function validateData($data)
    {
        if(!self::lengthValidation($data))
            return 'Invalid credit card number length';

    }

    /**
     * @param $data
     * @return bool
     */
    private function lengthValidation($data){
        $strlen = strlen(preg_replace('/\s+/', '', $data));
        return $strlen >=13 && $strlen <= 19 ? true : false ;
    }
}