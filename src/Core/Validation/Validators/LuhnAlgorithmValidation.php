<?php

namespace Core\Validation\Validators;
use Core\Validation\ValidationInterface;

/**
 * Class LuhnAlgorithmValidation
 * @package Core\Validation\Validators
 */
final class LuhnAlgorithmValidation implements ValidationInterface {

    /**
     * @param mixed $data
     * @return string|null
     */
    public function validateData($data)
    {
        if(!$this->luhnAlgorithm($data))
           return 'Invalid credit card data';

    }

    /**
     * @param $data
     * @return bool
     */
    private function luhnAlgorithm($data){

        $numberSum = 0;
        $data = str_split(strrev($data));
        foreach ( $data as $i => $d) {
            $numberSum += $i+1 % 2 !== 0 ? $d*2 : $d;
        }
        return $numberSum%10 == 0;
    }


}