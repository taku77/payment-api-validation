<?php
namespace Core\Validation\Validators;
use Core\Validation\ValidationInterface;


/**
 * Class CCV2Validation
 * @package Core\Validation\Validators
 */
final class CCV2Validation implements ValidationInterface {
    /**
     * @param mixed $data
     * @return string|null
     */
    public function validateData($data)
    {
        if(strlen(intval($data)) !== 3)
            return 'Invalid ccv2 length';

    }

}