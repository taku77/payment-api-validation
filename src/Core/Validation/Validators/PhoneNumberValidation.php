<?php
namespace Core\Validation\Validators;
use Core\Validation\ValidationInterface;

/**
 * Class PhoneNumberValidation
 * @package Core\Validation
 */
final class PhoneNumberValidation implements ValidationInterface {

    /**
     * {@inheritdoc}
     */
    public function validateData($data)
    {
        if(!preg_match('/^\+?([1-9]{2}|[0-9]{3})\s?([0-9]{9}|(([0-9]){3}\s?){3}|([0-9]{3}\s?([0-9]{2}\s?){2}))$/',$data))
            return 'Invalid phone number format';

    }


}