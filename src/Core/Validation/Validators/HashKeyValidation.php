<?php

namespace Core\Validation\Validators;
use Core\Api\KeyRepositoryInterface;
use Core\Validation\ValidationInterface;

/**
 * Class HashKeyValidation
 * @package Core\Validation\Validators
 */
final class HashKeyValidation implements ValidationInterface  {

    /**
     * @var KeyRepositoryInterface
     */
    private $keyRepository;
    public function __construct(KeyRepositoryInterface $keyRepository)
    {
        $this->keyRepository = $keyRepository;
    }

    /**
     * @param mixed $data
     */
    public function validateData($data)
    {
        if($this->checkHash($data))
            return true;

        return 'Invalid hash key';
    }


    /**
     * @param $data
     * @return string
     */
    public function generateHash($data){
        $newarray = array();
        array_walk_recursive($data,function ($val, $key) use (&$newarray){ $newarray[$key] = $val;});
        ksort($newarray);
        $string = '';
        $hash = '';
        $timestamp = '';
        foreach ($newarray as $key => $value){
            if($key == 'hash')
                $hash = $value;
            elseif ($key == 'timestamp')
                $timestamp = $value;
            else
                $string .= $value;
        }

        return sha1($string.$timestamp.$this->keyRepository->getKey());
    }
    /**
     * @param $data
     * @return bool
     */
    private function checkHash($data){
        $newarray = array();
        array_walk_recursive($data,function ($val, $key) use (&$newarray){ $newarray[$key] = $val;});
        ksort($newarray);
        $string = '';
        $hash = '';
        $timestamp = '';
        foreach ($newarray as $key => $value){
            if($key == 'hash')
                $hash = $value;
            elseif ($key == 'timestamp')
                $timestamp = $value;
            else
                $string .= $value;
        }

        return sha1($string.$timestamp.$this->keyRepository->getKey()) == $hash && $hash ;
    }
}