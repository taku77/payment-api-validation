<?php
namespace Core\Validation;

interface Validatable{
    /**
     * @return array
     */
    public function validate();
}