<?php
namespace Core\Api;
/**
 * Created by PhpStorm.
 * User: enixx
 * Date: 27.01.17
 * Time: 15:40
 */

interface KeyRepositoryInterface{
    /**
     * @param $key
     * @return mixed
     */
    public function checkKey($key);

    /**
     * @return string
     */
    public function getKey();
}