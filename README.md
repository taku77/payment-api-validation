payment-data-validation
=======================

Hash KEY
====
Hash key is generated from all parameters ordered alphabetical + timestamp + key

Key is stored in DefaultKeyRepository class as *const*

TESTS
=====

To run test use command
>php bin/phpunit -c app/ src/

SAMPLE DATA
===========

Sample valid json request


```
#!json

{
  "transactiondata": {
       "credit_card": {
          "card_number": "4485297189383563",
          "ccv2" : 123,
         "expiration_date" : "09/19"
        },
        "contact_data":{
          "phone_number": "555555555",
          "mobile" : "234233234",
          "email" : "example@example.com"
        }
      },
      "hash": "130b845b990fb4d2101e33053b30219fad228c0c",
      "timestamp" : "2017-02-01T18:25:43.511Z"
 }
```


 
 Sample xml valida data request
```
#!xml
 <?xml version="1.0" encoding="UTF-8" ?>
 <form>
     <transactiondata>
         <contactData>
             <email>example@example.com</email>
             <phoneNumber>555555555</phoneNumber>
             <mobile>234233234</mobile>
         </contactData>
         <creditCard>
             <cardNumber>4485297189383563</cardNumber>
             <expirationDate>09/19</expirationDate>
             <ccv2>123</ccv2>
         </creditCard>
         <hash>130b845b990fb4d2101e33053b30219fad228c0c</hash>
         <timestamp>2017-02-01T18:25:43.511Z</timestamp>
     </transactiondata>
 </form>
```
